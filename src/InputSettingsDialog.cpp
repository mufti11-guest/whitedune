/*
 * InputSettingsDialog.cpp
 *
 * Copyright (C) 1999 Stephen F. White/2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "stdafx.h"
#include "InputSettingsDialog.h"
#include "DuneApp.h"
#include "resource.h"
#include "swt.h"

InputSettingsDialog::InputSettingsDialog(SWND parent)
  : Dialog(parent, IDD_INPUT)
{
    LoadData();
}

void
InputSettingsDialog::OnCommand(int id)
{
    if (id == IDOK) {
        SaveData();
        if (Validate()) {
            swEndDialog(IDOK);
        }
    } else if (id == IDC_BROWSE_X3D_TO_X3DV) {
        char buffer[1024];
        const char *oldCommand = TheApp->GetX3d2X3dvCommand();
        if (oldCommand == NULL)
            oldCommand = "";
        mystrncpy_secure(buffer, oldCommand, 1024);
        if (TheApp->browseCommand(buffer, 1024, IDS_X3DV2X3D_PROMPT)) {
            TheApp->SetX3d2X3dvCommand(buffer);
            swSetText(swGetDialogItem(_dlg, IDC_X3D_TO_X3DV), 
                                      TheApp->GetX3d2X3dvCommand());
        }
    } else if (id == IDC_BROWSE_XML_PARSER_DETAILS) {
        OnHelpXmlParser();
    } else if (id == IDC_INPUT_DEFAULTS) {
        TheApp->InputSetDefaults();
        LoadData();
    } else if (id == IDCANCEL) {
        swEndDialog(IDCANCEL);
    }
}

void InputSettingsDialog::LoadData()
{
    swSetText(swGetDialogItem(_dlg, IDC_X3D_TO_X3DV), 
                              TheApp->GetX3d2X3dvCommand());
    swSetCheck(swGetDialogItem(_dlg, IDC_USE_INTERNAL_XML_PARSER), 
                               TheApp->useInternalXmlParser());
}

bool
InputSettingsDialog::Validate()
{
    return true;
}


void
InputSettingsDialog::SaveData() 
{
    char temp[1024];

    swGetText(swGetDialogItem(_dlg, IDC_X3D_TO_X3DV), temp, 1023);
    TheApp->SetX3d2X3dvCommand(temp);
    TheApp->setUseInternalXmlParser(swGetCheck(swGetDialogItem(_dlg, 
                                               IDC_USE_INTERNAL_XML_PARSER)));
}

#define XML_PARSER_DOC_URL "usage_docs/xml_parser.html"

void
InputSettingsDialog::OnHelpXmlParser()
{
    char path[4096];
    char buf[4096];
    
    char *url = swGetHelpUrl(TheApp->GetHelpBrowser());
    snprintf(path, 4095, "%s", url);
    char *lastSelector = strrchr(path, swGetPathSelector());
#ifdef _WIN32
    if (lastSelector == NULL)
        lastSelector = strrchr(path, '/');
#endif
    if (lastSelector == (path + strlen(path) - 1)) {
        // selector at end
        mysnprintf(path, 4095, "%s%s", url, XML_PARSER_DOC_URL);
    } else {
        char *lastDot = strrchr(url, '.');
        if (lastDot == NULL) {
            // no . (as in "something.html") => url "should" be directory
            mysnprintf(buf, 4095, "%s/%s", path, XML_PARSER_DOC_URL);
            strcpy(path, buf);
        } else {
            if (lastSelector == NULL) {
                // no selector but dot => url "should" be file
                mysnprintf(path, 4095, "%s/%s", url, XML_PARSER_DOC_URL); 
            } else {
                if (lastDot > lastSelector) {
                    *lastSelector = 0;
                }
                mysnprintf(buf, 4095, "%s/%s", path, XML_PARSER_DOC_URL);
                strcpy(path, buf);
            }
        }    
    }
    
    swHelpBrowserPath(TheApp->GetHelpBrowser(), path, _dlg);
}

