/*
 * NodeBoundedPhysicsModel.h
 *
 * Copyright (C) 1999 Stephen F. White
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_BoundedPhysicsModel_H
#define _NODE_BoundedPhysicsModel_H

#ifndef _NODE_H
#include "Node.h"
#endif
#ifndef _PROTO_MACROS_H
#include "ProtoMacros.h"
#endif
#ifndef _PROTO_H
#include "Proto.h"
#endif
#ifndef _MATRIX_H
#include "Matrix.h"
#endif

#include "SFMFTypes.h"

class ProtoBoundedPhysicsModel : public Proto {
public:
                    ProtoBoundedPhysicsModel(Scene *scene);

    virtual Node   *create(Scene *scene);
    FieldIndex      enabled;
    FieldIndex      geometry;
};

class NodeBoundedPhysicsModel : public BoundedPhysicsModelNode {
public:
                      NodeBoundedPhysicsModel(Scene *scene, Proto *proto);

protected:
                     ~NodeBoundedPhysicsModel();

public:
    virtual int       getType() const { return X3D_BOUNDED_PHYSICS_MODEL; }
// X3D profile changes addChildren, removeChildren
    virtual int       getProfile(void) const;
    virtual Node     *copy() const { return new NodeBoundedPhysicsModel(*this); }

    fieldMacros(SFBool, enabled,  ProtoBoundedPhysicsModel)
    fieldMacros(SFNode, geometry, ProtoBoundedPhysicsModel)
};

#endif
