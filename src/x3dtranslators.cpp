/*
 * x3dtranslators.cpp
 *
 * Copyright (C) 2001 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 of the License
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include "config.h"
#include "swt.h"
#include "DuneApp.h"
#include "x3dtranslators.h"
#include "MyString.h"
#include "resource.h"

#ifndef _WIN32
#include <stdlib.h>
#endif

static bool startCommand(const char *systemCommand, const char *what, 
                         const char *settings, 
                         const char* path, const char* filepath) {
    char command[1024*3];
    bool hasSystemCommand = true;
    if (systemCommand == NULL) 
        hasSystemCommand = false;
    if (hasSystemCommand)
        if (strlen(systemCommand) == 0)
            hasSystemCommand = false;
    if (!hasSystemCommand) {
        TheApp->MessageBox(IDS_NO_X3D_CONVERTER_SETTING, what, settings);
        return false;
    }
    mysnprintf(command, 1024*3-1, "%s %s %s", systemCommand, path, filepath);
    if (system(command) !=0) {
        TheApp->MessageBox(IDS_CONVERSION_TO_X3D_FAILED, path);
        return false;
    }
    return true;
}

/* convert x3d to VRML97 */

bool x3d2vrml(char* path,char* filepath) {
    return startCommand(TheApp->GetX3d2X3dvCommand(), 
                        "X3D(XML) to X3DV converter", 
                        "options->input settings...", filepath, path);
}


