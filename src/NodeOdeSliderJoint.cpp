/*
 * NodeOdeSliderJoint.cpp
 *
 * Copyright (C) 1999 Stephen F. White, 2007 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#include <stdio.h>
#ifndef FLT_MAX
# include <float.h>
#endif
#include "stdafx.h"

#include "NodeOdeSliderJoint.h"
#include "Proto.h"
#include "Field.h"
#include "FieldValue.h"
#include "MFNode.h"
#include "SFBool.h"
#include "DuneApp.h"
#include "Scene.h"
#include "RenderState.h"

ProtoOdeSliderJoint::ProtoOdeSliderJoint(Scene *scene)
  : ProtoSliderJoint(scene, "OdeSliderJoint")
{
    fMax.set(addExposedField(SFFLOAT, "fMax", new SFFloat(0)));
}

Node *
ProtoOdeSliderJoint::create(Scene *scene)
{ 
    return new NodeOdeSliderJoint(scene, this); 
}

NodeOdeSliderJoint::NodeOdeSliderJoint(Scene *scene, Proto *def)
  :NodeSliderJoint(scene, def)
{
}



