/*
 * NodeOdeSingleAxisHingeJoint.h
 *
 * Copyright (C) 1999 Stephen F. White, 2008 J. "MUFTI" Scheurich
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program (see the file "COPYING" for details); if 
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, 
 * Cambridge, MA 02139, USA.
 */

#ifndef _NODE_ODE_SINGLE_AXIS_HINGE_JOINT_H
#define _NODE_ODE_SINGLE_AXIS_HINGE_JOINT_H

#include "NodeSingleAxisHingeJoint.h"

#include "SFMFTypes.h"

class ProtoOdeSingleAxisHingeJoint : public ProtoSingleAxisHingeJoint {
public:
                    ProtoOdeSingleAxisHingeJoint(Scene *scene);

    virtual int     getType() const { return DUNE_ODE_SINGLE_AXIS_HINGE_JOINT; }

    virtual Node   *create(Scene *scene);

    FieldIndex fMax;
};

class NodeOdeSingleAxisHingeJoint : public NodeSingleAxisHingeJoint {
public:
                    NodeOdeSingleAxisHingeJoint(Scene *scene, Proto *proto);

    virtual Node   *copy() const { return new NodeOdeSingleAxisHingeJoint(*this); }

    fieldMacros(SFFloat, fMax, ProtoOdeSingleAxisHingeJoint)
};

#endif 
