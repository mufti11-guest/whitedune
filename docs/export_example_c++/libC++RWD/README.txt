This file is free software; the Free Software Foundation
gives unlimited permission to copy, distribute and modify it.


To use this example on Linux/UNIX/MacOSX, create a VRML/X3D file, export
it with white_dune as a C++(mesh) file named "C++Export.cc" into this directory 
and type

$ make

The resulting executable is named "./render"

To use this example on other systems, create a VRML/X3D file, export   
it with white_dune as a C++(mesh) file, compile and link it together with
libCRWD.c, main.c and the glut library

